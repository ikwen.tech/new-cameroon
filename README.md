# New Cameroon

New Cameroon is an open-source platform to build citizen centered services. The first released version in February
gives people the possibility to look for an ELECAM spot where to go and register on the elections file.


## Software stack

New Cameroon is a Python/Django application that runs on the following software:

- Ubuntu/Debian/RHEL/OpenSUSE/CentOS
- Python 3.8+
- Django 3.2+
- PostreSQL (Also works with MySQL or SQLite)
- Memcached

## ikwen software dependencies

New Cameroon is built on top of some ikwen libraries :
- ikwen

The installation section will tell you how to set everything up to get it running

## Install a development environment

This assumes you have basic knowledge of Django environment, how to start and run an app. You must install the following services first:
- PostreSQL
- Memcached

### Create a python virtual environment 

#### Install python virtualenv (skip if already installed)

    sudo apt install python3-venv -y

#### Create a virtual environment (skip if you already have one and prefer to use it)

    python3 -m venv /path/to/venv

The rest of the installation instructions assume that you have named your virtual environment `venv`.

#### Activate your virtualenv

    source /path/to/venv/bin/activate


### Install ikwen dependencies

Clone necessary ikwen libraires

#### Clone ikwen

    git clone https://gitlab.com/ikwen.tech/ikwen.git

#### Add the libraries to the virtual environment
To add libraries to the virtualenv, will juste create symlinks to `site-packages`. Replace the following paths with the one that matches your environment. Don't forget to replace `python3.x` by your own version.

#### Add ikwen to virtualenv
Create a link to the whole ikwen folder

    ln -sf /path/to/Projects/ikwen /path/to/venv/lib/python3.x/site-packages/ikwen

### Update the currencies app
The requirements you installed contain Django `currencies` application.
Some files need to be manually updated. Those are:
- currencies/models.py
- currencies/context_processors.py

An additional migration also needs to be added in `currencies/migrations`

#### Replace currencies models.py

    cp /path/to/Projects/ikwen/lib-hacks/currencies/models.py /path/to/venv/lib/python3.x/dist-packages/currencies/models.py

#### Replace currencies context_processors.py

    cp /path/to/Projects/ikwen/lib-hacks/currencies/context_processors.py /path/to/venv/lib/python3.x/dist-packages/currencies/context_processors.py

#### Add the additional migration

    cp /path/to/Projects/ikwen/lib-hacks/currencies/0007_currency_precision.py /path/to/venv/lib/python3.x/dist-packages/currencies/migrations/0007_currency_precision.py


### Install New Cameroon itself

#### Clone the project using SSH

    git clone git@gitlab.com:ikwen.tech/new-cameroon.git

OR

#### Clone the project using HTTPS
    
    git clone https://gitlab.com/ikwen.tech/new-cameroon.git

#### Install the requirements (tested in production)

    pip3 install requirements.txt

OR, you can be more adventurous and ...

#### Install the requirements (development)
This will install the latest versions of all requirements.
It is not guarantee that it will work. 

    pip3 install requirements-dev.txt


### Create a settings.ini

Create a `settings.ini` file in the `conf` folder, copy content from `conf/settings_tpl.ini`
and replace accordingly. **DO NOT ADD YOUR SETTINGS.INI FILE TO VERSIONING**. Your newly created
`settings.ini` requires some variables you need to generate beforehand.
- SECRET_KEY
- IKWEN_SERVICE_ID

#### Generate a Django SECRET_KEY and add to settings

    python3 manage.py generatesecretkey

#### Obtain an IKWEN_SERVICE_ID and add to settings

    python3 manage.py generateserviceid


### Create the database

    python3 manage.py migrate

### Create the superuser
This will also set up some basic records in the database by internally running `ikwen.core.tools.init_weblet()`

    python3 manage.py createsuperuser
    

### Run the Django development server

    python3 manage.py runserver


## Run New Cameroon in production

The easiest way is just to run a New Cameroon docker container. Once you have created a `settings.ini` file,
use it as a volume to pass you Django environment variables to the container.

    docker run -d -v /path/to/your/settings.ini:/Apps/New Cameroon/conf/settings.ini registry.gitlab.com/ikwen.tech/new-cameroon
