# This script is used as the docker CMD
# It starts uWSGI then Nginx right after

# Start uwsgi
uwsgi --ini web_servers/nginx_uwsgi/uwsgi.ini

# Start Nginx as foreground process
nginx -g "daemon off;"
