#!/bin/bash

# Create a backup of log files and empty current ones
# It's better to empty files because it maintains permissions set on it

cd "$DOCKER_RUN_HOME"/NewCameroon || exit

cp "$DOCKER_RUN_HOME"/NewCameroon/www/error.log "$DOCKER_RUN_HOME"/NewCameroon/www/error.bak.log
cat /dev/null > "$DOCKER_RUN_HOME"/NewCameroon/www/error.log
cp "$DOCKER_RUN_HOME"/NewCameroon/www/access.log "$DOCKER_RUN_HOME"/NewCameroon/www/access.bak.log
cat /dev/null > "$DOCKER_RUN_HOME"/NewCameroon/www/access.log
cp "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_info.log "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_info.bak.log
cat /dev/null > "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_info.log
cp "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_error.log "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_error.bak.log
cat /dev/null > "$DOCKER_RUN_HOME"/NewCameroon/log/ikwen_error.log


# CD to Traefik home and reload containers with docker-sed
cd "$TRAEFIK_HOME" || exit
docker-sed

# Let the containers reload first to make the
# migrations files available in it then run migrate
cd "$DOCKER_RUN_HOME"/NewCameroon || exit
TARGET_CONTAINER="traefik-newcameroon-1"
if [ "$(docker ps -q -f name="$TARGET_CONTAINER")" ]; then
  docker exec "$TARGET_CONTAINER" python3 /Apps/NewCameroon/manage.py migrate
fi
