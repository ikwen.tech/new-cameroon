FROM registry.gitlab.com/ikwen.tech/new-cameroon:base

COPY ikwen Apps/ikwen
COPY NewCameroon Apps/NewCameroon

RUN ln -sf /Apps/ikwen /usr/local/lib/python3.8/dist-packages/ikwen

# Change permissions so that Nginx can write to the uwsgi socket
# created as web_servers/nginx_uwsgi/uwsgi.sock
RUN chown -R root:www-data /Apps/NewCameroon
RUN chmod -R 775 /Apps/NewCameroon/web_servers/nginx_uwsgi

WORKDIR /Apps/NewCameroon

# Enable the site in Nginx
RUN ln -sf /Apps/NewCameroon/web_servers/nginx_uwsgi/nginx.conf /etc/nginx/sites-enabled/app.conf

EXPOSE 80

# Add execution mode to the startup script
RUN chmod +x web_servers/nginx_uwsgi/start_server.sh

CMD ["/bin/bash", "web_servers/nginx_uwsgi/start_server.sh"]
