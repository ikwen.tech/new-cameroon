import logging

from django.core.mail import send_mail
from django.urls import reverse
from django.utils.translation import ugettext as _
from ikwen.core.utils import get_service_instance

logger = logging.getLogger('ikwen')


def notify_new_spot_added(spot):
    service = get_service_instance()
    config = service.config
    recipient_list = [email.strip() for email in config.contact_email.split(',')]
    if not recipient_list:
        return

    spot_admin_url = f"{service.url}{reverse('admin:changespot_spot_change', args=(spot.id, ))}"
    message = _("A new spot was added\n\n"
                "City: %(city)s\n"
                "Hood: %(hood)s\n"
                "Place: %(place)s\n"
                "Category: %(category)s\n"
                "Start date: %(start_date)s\n"
                "End date: %(end_date)s.\n\n"
                "Check the spot\n"
                "%(spot_admin_url)s") % {'city': spot.hood.city.name, 'hood': spot.hood.name, 'place': spot.place,
                                         'category': spot.category, 'start_date': spot.start_date,
                                         'end_date': spot.end_date, 'spot_admin_url': spot_admin_url}
    send_mail(_("New ELECAM spot added"), message, "New Cameroon <new-cameroon@ikwen.com>", recipient_list)
