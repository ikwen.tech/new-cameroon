import json

from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from ikwen.billing.models import Donation
from ikwen.core.constants import CONFIRMED

from changespot.models import Spot, Hood, Citizen


class ChangeSpotTestCase(TestCase):
    fixtures = ['setup_data', 'ikwen_members', 'spots']

    @override_settings(IKWEN_SERVICE_ID='56eb6d04b37b3379b531b102', UNIT_TESTING=True)
    def test_home(self):
        """Make sure the pages open; returns http 200"""
        response = self.client.get(reverse('changespot:home'))
        self.assertEqual(response.status_code, 200)

    @override_settings(IKWEN_SERVICE_ID='56eb6d04b37b3379b531b102', UNIT_TESTING=True)
    def test_AddSpot(self):
        """Make sure the pages open; returns http 200"""
        response = self.client.get(reverse('changespot:add_spot'))
        self.assertEqual(response.status_code, 200)

        data = {
            'city': '663623ea9e32530f227b2d21',
            'hood': '__other__',
            'new_hood': 'Efoulan',
            'place': 'Efoulan Pont - Face Impôts',
            'category': 'Branch'
        }
        response = self.client.post(reverse('changespot:add_spot'), data=data)
        self.assertEqual(response.status_code, 302)
        hood = Hood.objects.get(name=data['new_hood'])
        Spot.objects.get(hood=hood)

    @override_settings(IKWEN_SERVICE_ID='56eb6d04b37b3379b531b102', UNIT_TESTING=True)
    def test_RemindMe(self):
        """Make sure the pages open; returns http 200"""
        response = self.client.get(reverse('changespot:remind_me'))
        self.assertEqual(response.status_code, 200)

        data = {
            'city': '663623ea9e32530f227b2d21',
            'hood': '__other__',
            'new_hood': 'Nlongkak',
            'place': 'Vallée',
            'expected_on': 'this-week',
            'phone': '684516023',
            'email': 'blaster@skytop.com'
        }
        response = self.client.post(reverse('changespot:remind_me'), data=data)
        self.assertEqual(response.status_code, 302)
        hood = Hood.objects.get(name=data['new_hood'])
        Citizen.objects.get(hood=hood, phone=data['phone'], email=data['email'])

    @override_settings(IKWEN_SERVICE_ID='56eb6d04b37b3379b531b102', UNIT_TESTING=True)
    def test_donate(self):
        """
        Checking out with Mobile Money should work well too
        """
        Donation.objects.all().delete()
        response = self.client.post(reverse('billing:momo_set_checkout'), {'amount': 2000})
        json_resp = json.loads(response.content)
        notification_url = json_resp['notification_url']
        payment_data = {
            "amount": 2000,
            "operator_code": "mtn-momo",
            "status": "Success",
            "operator_tx_id": "1234567890",
            "operator_user_id": "680805050",
            "message": "OK"
        }
        response = self.client.put(notification_url, payment_data, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        donation = Donation.objects.get()
        self.assertEqual(donation.status, CONFIRMED)
