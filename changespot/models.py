from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from ikwen.core.models import Model


class City(Model):
    name = models.CharField(_("name"), max_length=100, unique=True, db_index=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = _("Cities")
        ordering = ('name', )

    def __str__(self):
        return self.name


class Hood(Model):
    name = models.CharField(_("name of the hood"), max_length=100, db_index=True)

    city = models.ForeignKey(City, verbose_name=_("city"), on_delete=models.CASCADE)

    # Location
    location = models.PointField(null=True)

    is_active = models.BooleanField(_("active ?"), default=True, db_index=True,
                                    help_text=_("Activate to make it visible on the site."))

    # Search tags to retrieve this provider
    tags = models.CharField(max_length=100, db_index=True, null=True)

    class Meta:
        ordering = ('city__name', 'name', )
        unique_together = ('city', 'name', )

    def __str__(self):
        return f"{self.city} - {self.name}"


class Spot(Model):
    TEMPORARY = 'Tmp'
    BRANCH = 'Branch'
    CATEGORY_CHOICES = (
        (TEMPORARY, _("ELECAM descent on field")),
        (BRANCH, _("ELECAM Office")),
    )
    # Hood where the spot is (or will be)
    hood = models.ForeignKey(Hood, verbose_name=_("hood"), null=True, on_delete=models.CASCADE,
                             help_text=_("Hood where the spot is situated"))

    # Best description of where the spot is situated precisely (lieu-dit)
    place = models.CharField(_("place"), max_length=150, null=True,
                             help_text=_("Best description of where the spot is situated precisely."))

    # Category of spot.
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=30, db_index=True)

    # Date when the spot will start being available
    start_date = models.DateField(_("start date"), blank=True, null=True, db_index=True,
                                  help_text=_("Date when the spot starts being available."))

    # Date when the spot will leave
    end_date = models.DateField(_("end date"), blank=True, null=True, db_index=True,
                                help_text=_("Date when the spot stops being available."))

    # Location
    location = models.PointField(null=True)

    # Status
    status = models.IntegerField(default=0, db_index=True)

    is_active = models.BooleanField(_("active ?"), default=True, db_index=True,
                                    help_text=_("Activate to make it visible on the site."))

    # Search tags to retrieve this spot
    tags = models.CharField(max_length=100, db_index=True, null=True)

    def __str__(self):
        return f"{self.hood} - {self.category}"


class Citizen(Model):
    """
    Any person who would like to be reminded
    about an available ELECAM temporary spot.
    """
    name = models.CharField(max_length=100, blank=True, null=True)
    email = models.EmailField(null=True, db_index=True)
    phone = models.EmailField(null=True, db_index=True)

    # Hood where the citizen expects the spot to be
    hood = models.ForeignKey(Hood, null=True, on_delete=models.SET_NULL)

    # Location of the citizen when asking
    location = models.PointField(null=True)

    # Amount paid to receive reminder SMS
    amount = models.IntegerField(default=0)

    reminded_on = models.DateTimeField(blank=True, null=True, db_index=True)

    expected_on = models.DateTimeField(blank=True, null=True, db_index=True)

    def __str__(self):
        val = []
        if self.name:
            val.append(self.name)
        if self.phone:
            val.append(self.phone)
        if self.email:
            val.append(self.email)
        if self.hood:
            val.append(str(self.hood))
        val = " - ".join(val)
        return val
