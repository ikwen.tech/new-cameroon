import json
from datetime import timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import Distance
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView
from django.utils.translation import ugettext as _

from changespot.models import City, Hood, Spot, Citizen
from changespot.utils import notify_new_spot_added

donate_suggestions = [
    {'emote': "🤲", "amount": 100},
    {"emote": "😊", "amount": 500},
    {"emote": "😎", "amount": 1000},
    {"emote": "🤩", "amount": 2000},
    {"emote": "🥰", "amount": 5000},
    {"emote": "🎉", "amount": 10000}
]


class Home(TemplateView):
    template_name = 'changespot/home.html'

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        context['donate_suggestions'] = donate_suggestions
        return context

    def get(self, request, *args, **kwargs):
        action = request.GET.get('action')
        if action == 'list_cities':
            q = self.request.GET.get('q')
            city_list = []
            if q:
                max_chars = max(len(q) - 2, 3)
                city_list = [city.to_dict() for city in
                             City.objects.filter(name__icontains=q[:max_chars], is_active=True).order_by('name')]
            return HttpResponse(json.dumps(city_list), content_type='application/json')
        if action == 'list_hoods':
            city_id = request.GET.get('city_id')
            hood_qs = Hood.objects.filter(city=city_id, is_active=True)
            q = self.request.GET.get('q')
            if q:
                max_chars = max(len(q) - 2, 3)
                hood_qs = hood_qs.filter(name__icontains=q[:max_chars])
            hood_list = [hood.to_dict() for hood in hood_qs.order_by('name')]
            return HttpResponse(json.dumps(hood_list), content_type='application/json')
        elif action == 'list_spots':
            hood_id = request.GET.get('hood_id')
            spot_list = [spot.to_dict() for spot in
                         Spot.objects.filter(hood=hood_id, is_active=True).order_by('place')]
            return HttpResponse(json.dumps(spot_list), content_type='application/json')
        return super(Home, self).get(request, *args, **kwargs)


class AddSpot(TemplateView):
    template_name = 'changespot/add_spot.html'

    def get_context_data(self, **kwargs):
        context = super(AddSpot, self).get_context_data(**kwargs)
        context['spot_categories'] = Spot.CATEGORY_CHOICES
        object_id = self.request.GET.get('spot_id')
        if object_id:
            context['new_spot'] = get_object_or_404(Spot, pk=object_id)
        context['donate_suggestions'] = donate_suggestions
        return context

    def post(self, request, *args, **kwargs):
        post_data = request.POST.dict()
        new_hood = post_data.get('new_hood')
        if new_hood:
            post_data.pop('new_hood')
            hood = Hood.objects.create(city_id=post_data['city'], name=new_hood, is_active=False)
            post_data['hood'] = hood.id
        post_data['is_active'] = False
        hood = post_data.get('hood')
        place = post_data.get('place')
        category = post_data.get('category')

        context = self.get_context_data(**kwargs)
        if not (hood and place and category):
            context['errors'] = "Data error"
            return render(request, 'changespot/add_spot.html', context)

        spot = Spot(hood_id=hood, place=place, category=category, is_active=False)
        if category == Spot.TEMPORARY:
            start_date = post_data.get('start_date')
            end_date = post_data.get('end_date')
            if not (start_date and end_date):
                context['errors'] = "Data error"
                return render(request, 'changespot/add_spot.html', context)
            spot.start_date = start_date
            spot.end_date = end_date
        spot.save()
        notify_new_spot_added(spot)
        next_url = f"{reverse('changespot:add_spot')}?spot_id={spot.id}"
        messages.success(request, _(f"Spot <strong>{place}</strong> successfully added."))
        return HttpResponseRedirect(next_url)


class RemindMe(TemplateView):
    template_name = 'changespot/remind_me.html'

    def get_context_data(self, **kwargs):
        context = super(RemindMe, self).get_context_data(**kwargs)
        context['city_list'] = City.objects.filter(is_active=True)
        hood_id = self.request.GET.get('hood_id')
        if hood_id:
            hood = get_object_or_404(Hood, pk=hood_id)
            context['hood'] = hood
            context['hood_list'] = Hood.objects.filter(city=hood.city)
        context['donate_suggestions'] = donate_suggestions
        return context

    def post(self, request, *args, **kwargs):
        post_data = request.POST.dict()
        new_hood = post_data.get('new_hood')
        if new_hood:
            post_data.pop('new_hood')
            hood = Hood.objects.create(city_id=post_data['city'], name=new_hood, is_active=True)
            post_data['hood'] = hood.id
        hood = post_data.get('hood')
        email = post_data.get('email')
        phone = post_data.get('phone')
        expected_on = post_data.get('expected_on')
        now = timezone.now()
        days = 50  # next-2-months
        if expected_on == 'this-week':
            days = 5
        elif expected_on == 'next-week':
            days = 8
        elif expected_on == 'next-2-weeks':
            days = 15
        elif expected_on == 'next-month':
            days = 28
        expected_on = now + timedelta(days=days)

        context = self.get_context_data(**kwargs)
        if not hood:
            context['errors'] = "Data error"
            return render(request, 'changespot/remind_me.html', context)

        citizen = Citizen(hood_id=hood, email=email, phone=phone, expected_on=expected_on)
        citizen.save()
        next_url = f"{reverse('changespot:remind_me')}?citizen_id={citizen.id}"
        messages.success(request, _(f"Reminder successfully added."))
        return HttpResponseRedirect(next_url)


class NearestSpots(TemplateView):
    template_name = 'changespot/nearest_spots.html'

    def get(self, request, *args, **kwargs):
        action = request.GET.get('find_nearest_spots')
        if action == 'find_nearest_spots':
            lat = request.GET.get('lat')
            lng = request.GET.get('lng')
            max_radius = getattr(settings, 'MAX_RADIUS', 5)
            center = GEOSGeometry(f'POINT({lng} {lat})')
            queryset = Spot.objects.all()
            queryset = queryset.filter(location__distance_lt=(center, Distance(km=max_radius)))
            spot_list = [spot.to_dict() for spot in queryset]
            return HttpResponse(json.dumps(spot_list), content_type='application/json')
        return super(NearestSpots, self).get(request, *args, **kwargs)
