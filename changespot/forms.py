from django import forms
from changespot.models import Spot


class ChangeSpotForm(forms.ModelForm):
    class Meta:
        model = Spot
        fields = ['hood', 'place', 'category', 'start_date', 'end_date']
