from django.urls import path

from changespot.views import Home, AddSpot, RemindMe, NearestSpots

app_name = 'changespot'

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('add-spot', AddSpot.as_view(), name='add_spot'),
    path('remind-me', RemindMe.as_view(), name='remind_me'),
    path('nearest-spots', NearestSpots.as_view(), name='nearest_spots'),
]
