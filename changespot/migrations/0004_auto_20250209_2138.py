# Generated by Django 3.2.5 on 2025-02-09 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('changespot', '0003_auto_20250204_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='name',
            field=models.CharField(db_index=True, max_length=100, unique=True, verbose_name='name'),
        ),
        migrations.AlterUniqueTogether(
            name='hood',
            unique_together={('city', 'name')},
        ),
    ]
