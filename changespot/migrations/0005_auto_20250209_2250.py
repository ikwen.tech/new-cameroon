# Generated by Django 3.2.5 on 2025-02-09 21:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('changespot', '0004_auto_20250209_2138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='citizen',
            name='expected_on',
            field=models.DateTimeField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='citizen',
            name='reminded_on',
            field=models.DateTimeField(blank=True, db_index=True, null=True),
        ),
        migrations.AlterField(
            model_name='spot',
            name='end_date',
            field=models.DateField(blank=True, db_index=True, help_text='Date when the spot stops being available.', null=True, verbose_name='end date'),
        ),
        migrations.AlterField(
            model_name='spot',
            name='start_date',
            field=models.DateField(blank=True, db_index=True, help_text='Date when the spot starts being available.', null=True, verbose_name='start date'),
        ),
    ]
