from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from django.contrib.gis import admin as geoadmin
from ikwen.accesscontrol.admin import MemberAdmin
from ikwen.accesscontrol.models import Member
from import_export.admin import ImportExportMixin

from changespot.models import City, Hood, Spot, Citizen


class CityAdmin(admin.ModelAdmin):
    fields = ('name', 'is_active', )
    list_display = ('name', 'created_on', 'is_active', )
    list_filter = ('is_active', )
    ordering = ('name',)


class HoodAdmin(geoadmin.GeoModelAdmin):
    fields = ('city', 'name', 'is_active', )
    list_display = ('city', 'name', 'created_on', 'is_active', )
    list_filter = ('city', 'is_active')
    ordering = ('name', )


class SpotAdmin(ImportExportMixin, geoadmin.GeoModelAdmin):
    list_display = ('hood', 'category', 'start_date', 'end_date', 'created_on', 'is_active', )
    list_filter = ('hood__city', 'category', 'is_active', )
    fields = ('hood', 'place', 'category', 'start_date', 'end_date', 'is_active', )
    ordering = ('hood__city', 'hood__name', )


class CitizenAdmin(ImportExportMixin, geoadmin.GeoModelAdmin):
    list_display = ('email', 'phone', 'hood', 'expected_on', 'created_on')
    fields = ('name', 'email', 'phone', 'hood', 'amount', 'expected_on', 'reminded_on', )
    readonly_fields = ('name', 'email', 'phone', 'hood', 'amount', 'expected_on', 'reminded_on')
    list_filter = ('hood__city', )
    ordering = ('-id', )


admin.site.register(City, CityAdmin)
admin.site.register(Hood, HoodAdmin)
admin.site.register(Spot, SpotAdmin)
admin.site.register(Citizen, CitizenAdmin)

try:
    admin.site.register(Member, MemberAdmin)
except AlreadyRegistered:
    pass
