from django.apps import AppConfig


class ChangespotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'changespot'
