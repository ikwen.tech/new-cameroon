#!/bin/bash

APPS_DIR=~/Apps
IKWEN_DIR=~/Apps/ikwen
NEW_CAMEROON_DIR=~/Apps/NewCameroon

if [ ! -e $APPS_DIR ]; then
  mkdir $APPS_DIR
fi

if [ -e $IKWEN_DIR ]; then
  echo "Pulling ikwen"
  cd $IKWEN_DIR || exit
  git pull
else
  echo "Cloning ikwen"
  cd $APPS_DIR || exit
  git clone git@gitlab.com:ikwen.tech/ikwen.git
fi

if [ -e $NEW_CAMEROON_DIR ]; then
  echo "Pulling NewCameroon"
  cd $NEW_CAMEROON_DIR || exit
  git pull
else
  echo "Cloning NewCameroon"
  cd $APPS_DIR || exit
  git clone git@gitlab.com:ikwen.tech/new-cameroon.git NewCameroon
fi

if [ $# -gt 0 ]; then
  if [ "$1" == 'build-base' ]; then
    cd $APPS_DIR || exit
    docker build -t registry.gitlab.com/ikwen.tech/new-cameroon:base --file NewCameroon/DockerfileBase .
    docker push registry.gitlab.com/ikwen.tech/new-cameroon:base
    exit 0
  else
    echo "Unknown argument"
    exit 1
  fi
fi

cd ~ || exit

# Copy static files to the CDN Server
scp -r $NEW_CAMEROON_DIR/static/newcameroon "$CDN_USERNAME"@"$CDN_HOST":"$CDN_STATIC_ROOT"

# Push container to registry
cd $APPS_DIR || exit
docker build -t registry.gitlab.com/ikwen.tech/new-cameroon --file NewCameroon/Dockerfile .
docker push registry.gitlab.com/ikwen.tech/new-cameroon
