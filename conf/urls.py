from django.contrib.auth.views import LogoutView
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin

from changespot.views import Home

admin.autodiscover()

logout_redirect_url = getattr(settings, "LOGOUT_REDIRECT_URL", None)
if not logout_redirect_url:
    logout_redirect_url = getattr(settings, 'LOGIN_URL')

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('stelar/', admin.site.urls),
    path('logout', LogoutView.as_view(), {'next_page': logout_redirect_url}),
    path('ikwen/', include('ikwen.core.urls', namespace='ikwen')),
    path('billing/', include('ikwen.billing.urls', namespace='billing')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += i18n_patterns(
    path('', Home.as_view(), name='home'),
    path('', include('changespot.urls')),
)
